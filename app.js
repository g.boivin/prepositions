const CASES = {
    AKK: "Akkusativ",
    DAT: "Dativ",
    GEN: "Genitiv",
    MIX: "Akkusativ | Dativ"
};

const prepositions = {
    "durch":  CASES.AKK,
    "für": CASES.AKK,
    "gegen": CASES.AKK,
    "ohne": CASES.AKK,
    "um": CASES.AKK,
    "bis": CASES.AKK,
    "entlang": CASES.AKK,
    "wider": CASES.AKK,
    "aus": CASES.DAT,
    "bei": CASES.DAT,
    "nach": CASES.DAT,
    "seit": CASES.DAT,
    "mit": CASES.DAT,
    "von": CASES.DAT,
    "zu": CASES.DAT,
    "außer": CASES.DAT,
    "entgegen": CASES.DAT,
    "gegenüber": CASES.DAT,
    "außerhalb": CASES.GEN,
    "innerhalb": CASES.GEN,
    "trotz": CASES.GEN,
    "während": CASES.GEN,
    "wegen": CASES.GEN,
    "abseits": CASES.GEN,
    "diesseits": CASES.GEN,
    "jenseits": CASES.GEN,
    "angesichts": CASES.GEN,
    "anhand": CASES.GEN,
    "anlässlich": CASES.GEN,
    "anstelle": CASES.GEN,
    "infolge": CASES.GEN,
    "laut": CASES.GEN,
    "mangels": CASES.GEN,
    "mithilfe": CASES.GEN,
    "statt": CASES.GEN,
    "anstatt": CASES.GEN,
    "aufgrund": CASES.GEN,
    "zwecks": CASES.GEN,
    "an": CASES.MIX,
    "auf": CASES.MIX,
    "hinter": CASES.MIX,
    "in": CASES.MIX,
    "neben": CASES.MIX,
    "über": CASES.MIX,
    "unter": CASES.MIX,
    "vor": CASES.MIX,
    "zwischen": CASES.MIX,
};

let currentIndex = 0;
const errors = new Set();

const shuffledPrepositions = shuffle(Object.keys(prepositions)); 

function getCurrentPreposition() {
    const preposition = shuffledPrepositions[currentIndex];
    updatePagination();

    if(currentIndex === shuffledPrepositions.length) {
        showResults();
    }

    currentIndex++;

    const cases = document.querySelectorAll('[data-case]');
    cases.forEach(c => c.className = '');

    return preposition;
}

function setPreposition() {
    const prepo = document.getElementById('prepo-text');
    let newPrepo = getCurrentPreposition();
    while(newPrepo === prepo.innerText) {
        newPrepo = getCurrentPreposition();
    }
    prepo.innerText = newPrepo;
}

setPreposition();

document.getElementById('cases').addEventListener('click', clickHandler);
document.addEventListener('keyup', keyHandler);


function clickHandler({ target }) {
    const { case: causus } = target.dataset;
    if (causus) {
        console.log(causus)
        const prepo = document.getElementById('prepo-text').innerText;
        if (causus === prepositions[prepo]) {
            setPreposition();
        } else {
            if(!errors.has(prepo)) createError(prepo);
            errors.add(prepo);
            target.classList.add('error')
        }
    }
}

function keyHandler({ key }) {
    const keyToCausus = {
        "ArrowUp": CASES.AKK,
        "ArrowLeft": CASES.MIX,
        "ArrowRight": CASES.DAT,
        "ArrowDown": CASES.GEN,
    };

    const causus = keyToCausus[key];
    if (causus) {
        const prepo = document.getElementById('prepo-text').innerText;
        if (causus === prepositions[prepo]) {
            setPreposition();
        } else {
            if(!errors.has(prepo)) createError(prepo);
            errors.add(prepo);
            const target = document.querySelector(`[data-case='${causus}']`);
            target.classList.add('error')
        }
    }
}

function showResults() {
    const results = document.getElementById('results');
    document.getElementById('percentage').innerText = `${Math.floor((shuffledPrepositions.length - errors.size) / shuffledPrepositions.length * 100)}%`
    document.getElementById('quizz').classList.toggle('hidden');
    results.classList.toggle('hidden');
}

function createError(prepo) {
    document.getElementById('has-errors').className = '';
    document.getElementById('bravo').className = 'hidden';
    const error = document.createElement('div');
    error.className = 'error';
    error.innerText = `${prepo} + `;
    const strong = document.createElement('strong');
    strong.innerText = prepositions[prepo];
    error.append(strong);
    document.getElementById('errors').append(error);
}

function updatePagination() {
    if (currentIndex > 0 ) {
        const pagination = document.getElementById('pagination');
        const span = document.createElement('span');
        span.className = errors.has(shuffledPrepositions[currentIndex - 1]) ? 'bad' : 'good';
        span.setAttribute("style", `flex: calc(1/${shuffledPrepositions.length})`);
        //pagination.innerText = `${currentIndex + 1}/${shuffledPrepositions.length}`;
        pagination.append(span);
    }
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}